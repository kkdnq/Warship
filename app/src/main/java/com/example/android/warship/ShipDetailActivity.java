package com.example.android.warship;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by hasee on 2017/8/9.
 */

public class ShipDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_detail);

        //获取要显示的数据的BIANHAO
        Intent intent = getIntent();
        String bianhao = intent.getStringExtra("BIANHAO");
        //显示数据
        setData(bianhao);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    //右上角菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //返回按钮
            case R.id.detail_back:
                int resultCode0 = 10;
                Intent intent0 = new Intent();
                intent0.putExtra("result","back to list");
                setResult(resultCode0,intent0);
                finish();
                break;
            //编辑按钮
            case R.id.detail_edit:
                Intent intent3 = new Intent(ShipDetailActivity.this,ShipEditActivity.class);
                String bianhao3 = ((TextView) findViewById(R.id.detail_bianhao)).getText().toString();
                intent3.putExtra("BIANHAO",bianhao3);
                startActivity(intent3);
                finish();
                break;
            //删除按钮
            case R.id.detail_delete:
                String bianhao = ((TextView) findViewById(R.id.detail_bianhao)).getText().toString();
                deleteData(bianhao);

                int resultCode1 = 20;
                Intent intent1 = new Intent();
                intent1.putExtra("result","delete");
                setResult(resultCode1,intent1);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //显示数据
    private void setData(String bianhao) {
        //从数据库获得BIANHAO对应的数据cursor
        String columns = "*";
        String tables = "ships";
        String ranges = bianhao;
        DbOperation operation = new DbOperation();
        Cursor cursor = operation.read(columns,tables,bianhao,null);

        //循环读取cursor中的数据并put到DetailActivity中
        if (!cursor.moveToFirst()){
        }else {
            String editData[] = getResources().getStringArray(R.array.dataList);
            int columnIndex = 0;
            String value = "";
            int id = 0;
            //put edit 数据
            for (int i = 1; i < editData.length; i++) {
                columnIndex = cursor.getColumnIndex(editData[i]);
                value = cursor.getString(columnIndex);
                id = getResources().getIdentifier("detail_"+editData[i].toLowerCase(),"id",getPackageName());
                ((TextView)findViewById(id)).setText(value);
            }

            //put 图片
            int BmpColumnIndex = cursor.getColumnIndex("PICTURE");
            byte[] picture = cursor.getBlob(BmpColumnIndex);
            Bitmap bitmap = BitmapFactory.decodeByteArray(picture,0,picture.length);
            ((ImageView) findViewById(R.id.detail_pic)).setImageBitmap(bitmap);
        }
        cursor.close();
    }

    //删除数据
    private void deleteData(String bianhao) {
        String tables = "ships";
        DbOperation operation = new DbOperation();
        operation.delete(tables,bianhao);
    }

}
