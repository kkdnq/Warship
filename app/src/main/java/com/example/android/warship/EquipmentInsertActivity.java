package com.example.android.warship;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by hasee on 2017/9/4.
 */

public class EquipmentInsertActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment_insert);

        //绑定OnclickListener来插入图片
        ((ImageView) findViewById(R.id.pic_eq)).setOnClickListener(picListener);

        //填充shecheng Spinner
        String shecheng[] = getResources().getStringArray(R.array.shecheng);
        ArrayList shecheng_list = new ArrayList<String>();
        for (int i = 0; i < shecheng.length; i++) {
            shecheng_list.add(shecheng[i]);
        }
        ((Spinner) findViewById(R.id.insert_eq_shecheng)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shecheng_list));

        //填充zhuangbeizhonglei Spinner
        String zhuangbeizhonglei[] = getResources().getStringArray(R.array.zhuangbeizhonglei);
        ArrayList zhuangbeizhonglei_list = new ArrayList<String>();
        for (int i = 0; i < zhuangbeizhonglei.length; i++) {
            zhuangbeizhonglei_list.add(zhuangbeizhonglei[i]);
        }
        ((Spinner) findViewById(R.id.insert_eq_zhuangbeizhonglei)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, zhuangbeizhonglei_list));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_eq_insert, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //右上角菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //确认 创建一条数据 按钮
            case R.id.insert_eq_done:
                insert();
                int resultCode0 = 210;
                Intent intent0 = new Intent();
                intent0.putExtra("result", "done");
                setResult(resultCode0, intent0);
                finish();
                break;
            //取消创建数据并返回ListActivity
            case R.id.insert_eq_cancel:
                int resultCode1 = 211;
                Intent intent1 = new Intent();
                intent1.putExtra("result", "cancel");
                setResult(resultCode1, intent1);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //创建一条数据
    private void insert() {
        int id = 0;
        //获取数据并打包
        ContentValues values = new ContentValues();
        String eqEditList[] = getResources().getStringArray(R.array.eqEditList);
        String eqSpinnerList[] = getResources().getStringArray(R.array.eqSpinnerList);
        //获取edit数据
        for (int i = 0;i < eqEditList.length;i ++){
            id = getResources().getIdentifier("insert_eq_"+eqEditList[i],"id",getBaseContext().getPackageName());
            String value = ((EditText) findViewById(id)).getText().toString();
            if (value != null && value.length() != 0){
                values.put(eqEditList[i].toUpperCase(),value);
            }
        }
        //获取spinner数据
        for (int i = 0;i < eqSpinnerList.length;i ++){
            id = getResources().getIdentifier("insert_eq_"+eqSpinnerList[i],"id",getBaseContext().getPackageName());
            String value = ((Spinner) findViewById(id)).getSelectedItem().toString();
            if (value != null && value.length() != 0){
                values.put(eqSpinnerList[i].toUpperCase(),value);
            }
        }
        //获取图片数据
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        Bitmap bmp = ((BitmapDrawable)((ImageView) findViewById(R.id.pic_eq)).getDrawable()).getBitmap();
        bmp.compress(Bitmap.CompressFormat.PNG,100,bao);
        bao.toByteArray();
        values.put("ICON",bao.toByteArray());

        //插入数据
        DbOperation operation = new DbOperation();
        operation.insert("equipments",values);
    }

    private View.OnClickListener picListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String bianhao = ((EditText) findViewById(R.id.insert_eq_bianhao)).getText().toString();
            GetPic getPic = new GetPic();
            if (bianhao == null || bianhao.length() == 0) {
                Toast.makeText(getApplicationContext(), "请正确输入编号", Toast.LENGTH_SHORT);
            } else {
                Bitmap bmp = getPic.getPic(R.id.pic_eq, bianhao);
                if (bmp == null) {
                    Toast.makeText(getApplicationContext(), "图片不存在！", Toast.LENGTH_SHORT);
                } else {
                    ((ImageView) findViewById(R.id.pic_eq)).setImageBitmap(bmp);
                }
            }
        }
    };
}
