package com.example.android.warship;

import android.graphics.Bitmap;

/**
 * Created by hasee on 2017/8/9.
 */

//自定义数据类型
public class ShipEquip {
    private Bitmap mBitmap;
    private String[] mListData;

    public ShipEquip(Bitmap bitmap, String[] listData) {
        mBitmap = bitmap;
        mListData = listData;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public String[] getListData() {
        return mListData;
    }
}
