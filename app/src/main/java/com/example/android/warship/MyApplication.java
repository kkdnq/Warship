package com.example.android.warship;

import android.app.Application;
import android.content.Context;

/**
 * Created by hasee on 2017/8/10.
 */

public class MyApplication extends Application {
    private static Context mContext;
    @Override
    public void onCreate(){
        super.onCreate();
        mContext = getApplicationContext();
    }
    public static Context getGlobalContext(){
        return mContext;
    }
}
