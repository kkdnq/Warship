package com.example.android.warship;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hasee on 2017/8/9.
 */

public class ShipAdapter extends ArrayAdapter<ShipEquip> {
    private static final String LOG_TAG = ShipAdapter.class.getSimpleName();

    public ShipAdapter(Activity context, ArrayList<ShipEquip> arrayList) {
        super(context, 0, arrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        ShipEquip currentShipEquip = getItem(position);
        //填充图片
        ImageView icon = (ImageView) listItemView.findViewById(R.id.pic_2);
        icon.setImageBitmap(currentShipEquip.getBitmap());
        //填充其他数据
        String[] listData = currentShipEquip.getListData();
        int id = 0;
        String otherData[] = MyApplication.getGlobalContext().getResources().getStringArray(R.array.showInList);
        for (int i = 0; i < listData.length - 1; i++) {
            id = getContext().getResources().getIdentifier("list_" + otherData[i].toLowerCase(), "id", getContext().getPackageName());
            ((TextView) listItemView.findViewById(id)).setText(listData[i]);
        }
        //修正颜色
        String xingji = listData[listData.length - 1];
        int colorId = getContext().getResources().getIdentifier("color_"+xingji,"color",getContext().getPackageName());
        int color = ContextCompat.getColor(getContext(),colorId);
        ((LinearLayout) listItemView.findViewById(R.id.list_layout0)).setBackgroundColor(color);

        return listItemView;
    }
}
