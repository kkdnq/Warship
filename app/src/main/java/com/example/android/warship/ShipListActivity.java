package com.example.android.warship;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ShipListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_list);

        //绑定Adapter
        String orders = "BIANHAO";
        ((ListView) findViewById(R.id.list)).setAdapter(new ShipAdapter(this, getList(orders)));

        //设置OnClickListener
        ((ListView) findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String bianhao = ((TextView) view.findViewById(R.id.list_bianhao)).getText().toString();
                Intent intent = new Intent(ShipListActivity.this, ShipDetailActivity.class);
                intent.putExtra("BIANHAO", bianhao);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    protected void onResume() {
        //绑定Adapter
        String orders = "BIANHAO";
        ((ListView) findViewById(R.id.list)).setAdapter(new ShipAdapter(this, getList(orders)));

        super.onResume();
    }

    private ArrayList<ShipEquip> getList(String orders) {
        ArrayList<ShipEquip> list = new ArrayList<ShipEquip>();
        String[] otherData = getResources().getStringArray(R.array.showInList);
        int columnIndex = 0;
        //从数据库中获取cursor
        String tables = "ships";
        String columns = "BIANHAO,JIANZHONG,JIANMING,HANGSU,HUOLI,ZHUANGJIA,NAIJIU,XINGYUN,XINGJI,ICON";
        DbOperation operation = new DbOperation();
        Cursor cursor = operation.read(columns,tables,null,orders);
        //将数据从cursor中读取出来并存入arrayList
        if (!cursor.moveToFirst()) {
        } else {
            do {
                //获取其他数据
                String[] listData = new String[9];
                for (int i = 0; i < 9; i++) {
                    columnIndex = cursor.getColumnIndex(otherData[i]);
                    listData[i] = cursor.getString(columnIndex);
                }
                //获取头像
                columnIndex = cursor.getColumnIndex("ICON");
                byte[] icon = cursor.getBlob(columnIndex);
                Bitmap bmp = BitmapFactory.decodeByteArray(icon, 0, icon.length);
                //数据加入ArrayList
                list.add(new ShipEquip(bmp, listData));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }
    //右上角菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //增加数据
            case R.id.list_add:
                int requestCode = 1;
                Intent intent = new Intent(ShipListActivity.this, ShipInsertActivity.class);
                startActivityForResult(intent, requestCode);
                break;
            //转到装备列表
            case R.id.list_swap:
                Intent intent1 = new Intent(ShipListActivity.this, EquipmentListActivity.class);
                startActivity(intent1);
                finish();
                break;
            //排序显示
            case R.id.list_order:
                final String orderList[] = getResources().getStringArray(R.array.orderList);
                final Boolean boList[] = new Boolean[orderList.length];
                for (int i = 0;i < orderList.length;i ++){
                    boList[i] = false;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("ORDER BY");
                builder.setMultiChoiceItems(orderList,null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        boList[which] = isChecked;
                    }
                });
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String order = "";
                        for (int i = 0;i < orderList.length;i ++){
                            if (boList[i]){
                                order += orderList[i] + ",";
                            }
                        }
                        order = order.substring(0,order.length() - 1);
                        ((ListView) findViewById(R.id.list)).setAdapter(new ShipAdapter(ShipListActivity.this, getList(order)));
//                        Log.v("sssssssssssssssssssss",""+order);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                break;
            //计算器
            case R.id.list_calculate:
                Intent intentc = new Intent(ShipListActivity.this, Exp.class);
                startActivity(intentc);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
