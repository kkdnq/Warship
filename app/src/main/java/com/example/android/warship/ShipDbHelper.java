package com.example.android.warship;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hasee on 2017/8/8.
 */

public class ShipDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "warship.db";
    private static final int DATABASE_VERSION = 1;

    public ShipDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建ship表
        String SQL_CREATE_SHIP_TABLE = "CREATE TABLE ships (" +
                "XINGJI TEXT NOT NULL, " +
                "BIANHAO TEXT NOT NULL, " +
                "JIANMING TEXT NOT NULL, " +
                "JIANZHONG TEXT NOT NULL, " +
                "HANGSU TEXT NOT NULL, " +
                "HUOLI TEXT NOT NULL, " +
                "ZHUANGJIA TEXT NOT NULL, " +
                "NAIJIU TEXT NOT NULL, " +
                "XINGYUN TEXT NOT NULL, " +
                "SHECHENG TEXT NOT NULL, " +
                "LEIZHUANG TEXT NOT NULL, " +
                "SHANBI TEXT NOT NULL, " +
                "DUIQIAN TEXT NOT NULL, " +
                "DUIKONG TEXT NOT NULL, " +
                "SUODI TEXT NOT NULL, " +
                "JIANZAOSHIJIAN TEXT NOT NULL, " +
                "DANYAO TEXT NOT NULL, " +
                "YOULIANG TEXT NOT NULL, " +
                "GOULIANGKOUGAN TEXT NOT NULL, " +
                "FANMAIHUISHOU TEXT NOT NULL, " +
                "DAZAILIANG TEXT NOT NULL, " +
                "DAZAIFENBU TEXT NOT NULL, " +
                "ZHUANGBEI1 TEXT NOT NULL, " +
                "ZHUANGBEI2 TEXT NOT NULL, " +
                "ZHUANGBEI3 TEXT NOT NULL, " +
                "ZHUANGBEI4 TEXT NOT NULL, " +
                "PICTURE BLOB NOT NULL, " +
                "ICON BLOB NOT NULL );";
        db.execSQL(SQL_CREATE_SHIP_TABLE);
        //创建equipment表
        String SQL_CREATE_EQUIPMENT_TABLE = "CREATE TABLE equipments (" +
                "BIANHAO TEXT NOT NULL, " +
                "ZHUANGBEIMING TEXT NOT NULL, " +
                "ZHUANGBEIZHONGLEI TEXT NOT NULL, " +
                "XINGJI TEXT NOT NULL, " +
                "SHECHENG TEXT NOT NULL, " +
                "HUOLI TEXT NOT NULL, " +
                "ZHUANGJIA TEXT NOT NULL, " +
                "LEIZHUANG TEXT NOT NULL, " +
                "BAOZHUANG TEXT NOT NULL, " +
                "DUIKONG TEXT NOT NULL, " +
                "DUIQIAN TEXT NOT NULL, " +
                "SHANBI TEXT NOT NULL, " +
                "MINGZHONG TEXT NOT NULL, " +
                "SUODI TEXT NOT NULL, " +
                "XINGYUN TEXT NOT NULL, " +
                "DUIKONGBUZHENG TEXT NOT NULL, " +
                "LVYONGLIANG TEXT NOT NULL, " +
                "KAIFASHIJIAN TEXT NOT NULL, " +
                "ICON BLOB NOT NULL );";
        db.execSQL(SQL_CREATE_EQUIPMENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
