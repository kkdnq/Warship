package com.example.android.warship;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by hasee on 2017/9/4.
 */

public class EquipmentAdapter extends ArrayAdapter<ShipEquip> {

    public EquipmentAdapter(Activity context, ArrayList<ShipEquip> arrayList) {
        super(context, 0, arrayList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.eq_list_item,parent,false);
        }

        ShipEquip currentShipEquip = getItem(position);

        //填充图片
        ((ImageView) listItemView.findViewById(R.id.pic_3)).setImageBitmap(currentShipEquip.getBitmap());

        //填充其他数据
        int id = 0;
        String listData[] = currentShipEquip.getListData();
        ((TextView)listItemView.findViewById(R.id.list_eq_bianhao)).setText(listData[0]);
        ((TextView)listItemView.findViewById(R.id.list_eq_zhuangbeiming)).setText(listData[1]);
        ((TextView)listItemView.findViewById(R.id.list_eq_zhuangbeizhonglei)).setText(listData[2]);

        //修正颜色
        String xingji = listData[listData.length - 1];
        int colorId = getContext().getResources().getIdentifier("color_"+xingji,"color",getContext().getPackageName());
        int color = ContextCompat.getColor(getContext(),colorId);
        ((LinearLayout) listItemView.findViewById(R.id.eq_list_layout0)).setBackgroundColor(color);

        return listItemView;
    }
}
