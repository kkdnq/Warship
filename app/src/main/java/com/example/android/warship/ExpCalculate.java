package com.example.android.warship;

/**
 * Created by hasee on 2017/9/5.
 */

public class ExpCalculate {
    private int[] eExp = new int[100];  //每一级所需经验

    //计算单次出征经验，总经验，出征次数
    public int[] calculate(int startLv, int endLv, int stageExp, double evaluate, double flagship, int mvp, double build, double extraAdd) {
        int mStartLv = startLv;        //初始等级
        int mEndLv = endLv;            //最终等级
        int mStageExp = stageExp;      //关卡经验系数
        double mEvaluate = evaluate;   //关卡评价系数
        double mFlagship = flagship;   //旗舰系数
        int mMvp = mvp;                //mvp系数
        double mBuild = build;         //圣肝系数
        double mExtraAdd = extraAdd;   //宁海系数

        double number = 0;    //所需出征次数
        int totalExp = 0;   //舰娘升级总需经验值
        double linerExp = 0;   //舰娘单次出征所得经验值

        //等级对应经验
        eExp[0] = 0;
        eExp[1] = 100;
        eExp[2] = 300;
        eExp[3] = 600;
        eExp[4] = 1000;
        eExp[5] = 1500;
        eExp[6] = 2100;
        eExp[7] = 2800;
        eExp[8] = 3600;
        eExp[9] = 4500;
        eExp[10] = 5500;
        eExp[11] = 6600;
        eExp[12] = 7800;
        eExp[13] = 9100;
        eExp[14] = 10500;
        eExp[15] = 12000;
        eExp[16] = 13600;
        eExp[17] = 15300;
        eExp[18] = 17100;
        eExp[19] = 19000;
        eExp[20] = 21000;
        eExp[21] = 23100;
        eExp[22] = 25300;
        eExp[23] = 27600;
        eExp[24] = 30000;
        eExp[25] = 32500;
        eExp[26] = 35100;
        eExp[27] = 37800;
        eExp[28] = 40600;
        eExp[29] = 43500;
        eExp[30] = 46500;
        eExp[31] = 49600;
        eExp[32] = 52800;
        eExp[33] = 56100;
        eExp[34] = 59500;
        eExp[35] = 63000;
        eExp[36] = 66600;
        eExp[37] = 70300;
        eExp[38] = 74100;
        eExp[39] = 78000;
        eExp[40] = 82000;
        eExp[41] = 86100;
        eExp[42] = 90300;
        eExp[43] = 94600;
        eExp[44] = 99000;
        eExp[45] = 103500;
        eExp[46] = 108100;
        eExp[47] = 112800;
        eExp[48] = 117600;
        eExp[49] = 122500;
        eExp[50] = 127500;
        eExp[51] = 132700;
        eExp[52] = 138100;
        eExp[53] = 143700;
        eExp[54] = 149500;
        eExp[55] = 155500;
        eExp[56] = 161700;
        eExp[57] = 168100;
        eExp[58] = 174700;
        eExp[59] = 181500;
        eExp[60] = 188500;
        eExp[61] = 195800;
        eExp[62] = 203400;
        eExp[63] = 211300;
        eExp[64] = 219600;
        eExp[65] = 228300;
        eExp[66] = 237400;
        eExp[67] = 247000;
        eExp[68] = 257100;
        eExp[69] = 267800;
        eExp[70] = 279100;
        eExp[71] = 291100;
        eExp[72] = 303800;
        eExp[73] = 317300;
        eExp[74] = 331700;
        eExp[75] = 347000;
        eExp[76] = 363300;
        eExp[77] = 380700;
        eExp[78] = 399300;
        eExp[79] = 419100;
        eExp[80] = 440200;
        eExp[81] = 462700;
        eExp[82] = 486700;
        eExp[83] = 512300;
        eExp[84] = 539600;
        eExp[85] = 568700;
        eExp[86] = 599700;
        eExp[87] = 632700;
        eExp[88] = 667900;
        eExp[89] = 705400;
        eExp[90] = 745300;
        eExp[91] = 787700;
        eExp[92] = 832800;
        eExp[93] = 880700;
        eExp[94] = 931600;
        eExp[95] = 985600;
        eExp[96] = 1042800;
        eExp[97] = 1103400;
        eExp[98] = 1167600;
        eExp[99] = 1235500;

        //计算单次出征经验，总经验，出征次数
        totalExp = eExp[mEndLv - 1] - eExp[mStartLv - 1];
        linerExp = mStageExp * mEvaluate * mFlagship * mMvp * mBuild * mExtraAdd;
        number = totalExp / linerExp;

        //设置输出数组
        int mLinerExp = (int) Math.round(linerExp);
        int mNumber = (int) Math.round(number);
        int[] outMessage = {mLinerExp, totalExp, mNumber};
        return outMessage;
    }
}