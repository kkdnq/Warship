package com.example.android.warship;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

/**
 * Created by hasee on 2017/9/5.
 */

public class Exp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exp_calculate);

        //设置焦点
        TextView lv = (TextView) findViewById(R.id.lv);
        lv.setFocusable(true);
        lv.setFocusableInTouchMode(true);
        lv.requestFocus();

        //输出信息
        TextView outPut = (TextView) findViewById(R.id.balance);
        outPut.setText("请先设置相关选项！");

        //设置计算按钮的onClickListener
        findViewById(R.id.button_calculate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //获得舰娘初始等级
                int startLv = getStartLv();
                //获得舰娘最终等级
                int endLv = getEndLv();
                //获得战斗评价系数
                double evaluateExp = getEvaluateExp();
                //获得旗舰系数
                double flagShip = getFlagShip();
                //获得MVP系数
                int mvp = getMvp();
                //获得圣肝系数
                double build = getBuild();
                //获得宁海系数
                double extraAdd = getExtraAdd();
                //获得关卡经验值
                int stageExp = getStageExp();

                //检测输入数据是否符合计算要求并定义一个ExpCalculate类并计算
                int[] getMessage = {0, 0, 0};     //用于储存计算结果，依次为单次出征经验，总经验，出征次数
                String stage = getStage();        //所刷关卡
                String evaluate = getEvaluate();  //战斗评价
                if (startLv > 0 && startLv < 101 && endLv > 0 && endLv < 101 && startLv < endLv) {
                    ExpCalculate expCalculate = new ExpCalculate();
                    getMessage = expCalculate.calculate(startLv, endLv, stageExp, evaluateExp, flagShip, mvp, build, extraAdd);
                } else {
                    //显示输入错误
                    TextView outPut = (TextView) findViewById(R.id.balance);
                    outPut.setText("请正确输入等级！");
                }

                //检测输出信息是否符合格式要求
                if (startLv > 0 && startLv < 101 && endLv > 0 && endLv < 101 && startLv < endLv) {
                    //更新输出信息
                    String outMessage = "";
                    outMessage = outMessage + "舰娘模式：" + "由等级" + startLv + "升至等级" + endLv + "," + "共需经验值" + getMessage[1] + "\n" + "刷" + stage + "关卡的单个节点，在战斗评价为" + evaluate + "的前提下，\n" + "一次经验值为： " + getMessage[0] + " ," + "所需次数为： " + getMessage[2] + " !";
                    TextView outPut = (TextView) findViewById(R.id.balance);
                    outPut.setText(outMessage);
                } else {
                    //显示输入错误
                    TextView outPut = (TextView) findViewById(R.id.balance);
                    outPut.setText("请正确输入等级！");
                }
            }
        });


    }


    //从EditText获得舰娘初始等级startLv
    public int getStartLv() {
        int mStartLv = 1;
        EditText startLv_view = (EditText) findViewById(R.id.start_lv);
        String startLvString = startLv_view.getText().toString().trim();
        if (startLvString != null && startLvString.length() != 0) {
            mStartLv = Integer.parseInt(startLvString);
            if (mStartLv == 0) {
                mStartLv = 1;
                startLv_view.setText("" + mStartLv);
            }
            else if (mStartLv >= 100) {
                mStartLv = 99;
                startLv_view.setText("" + mStartLv);
            }
        }

//        if (startLvString != null && startLvString.length() != 0) {
//            Pattern patten = Pattern.compile("[0-9]*");
//            if (patten.matcher(startLvString).matches()) {
//                mStartLv = Integer.parseInt(startLvString);
//            } else {
//                Toast.makeText(getApplicationContext(), "请输入数字", Toast.LENGTH_SHORT).show();
//            }
//        }

        return mStartLv;
    }

    //从EditText获得舰娘最终等级endLv
    public int getEndLv() {
        int mEndLv = 1;
        int mStartLv = getStartLv();
        EditText endLv_view = (EditText) findViewById(R.id.end_lv);
        String endLvString = endLv_view.getText().toString().trim();
        if (endLvString != null && endLvString.length() != 0) {
            mEndLv = Integer.parseInt(endLvString);
            if (mEndLv <= mStartLv) {
                mEndLv = mStartLv + 1;
                endLv_view.setText("" + mEndLv);
            }
            else if (mEndLv >100) {
                mEndLv = 100;
                endLv_view.setText("" + mEndLv);
            }
        }
//        if (endLvString != null && endLvString.length() != 0) {
//            Pattern pattern = Pattern.compile("[0-9]*");
//            if (pattern.matcher(endLvString).matches()) {
//                mEndLv = Integer.parseInt(endLvString);
//            } else {
//                Toast.makeText(getApplicationContext(), "请输入数字", Toast.LENGTH_SHORT).show();
//            }
//        }
        return mEndLv;

    }

    //从CheckBox中得到旗舰系数
    public double getFlagShip() {
        double mFlagShip = 0;
        CheckBox flagShipCheckBox = (CheckBox) findViewById(R.id.flag_ship);
        String flagShipCheck = "" + flagShipCheckBox.isChecked();
        switch (flagShipCheck) {
            case "true":
                mFlagShip = 1.5;
                break;
            case "false":
                mFlagShip = 1;
                break;
            default:
                break;
        }

        return mFlagShip;
    }

    //从CheckBox中得到MVP系数
    public int getMvp() {
        int mMvp = 0;
        CheckBox mvpCheckBox = (CheckBox) findViewById(R.id.mvp);
        String mvpCheck = "" + mvpCheckBox.isChecked();
        switch (mvpCheck) {
            case "true":
                mMvp = 2;
                break;
            case "false":
                mMvp = 1;
                break;
            default:
                break;
        }

        return mMvp;
    }

    //从CheckBox中得到圣肝系数
    public double getBuild() {
        double mBuild = 0;
        CheckBox buildCheckBox = (CheckBox) findViewById(R.id.build);
        String buildCheck = "" + buildCheckBox.isChecked();
        switch (buildCheck) {
            case "true":
                mBuild = 1.5;
                break;
            case "false":
                mBuild = 1;
                break;
            default:
                break;
        }

        return mBuild;
    }

    //从CheckBox中得到宁海系数
    public double getExtraAdd() {
        double mExtraAdd = 0;
        CheckBox extraAddCheckBox = (CheckBox) findViewById(R.id.extra_add);
        String extraAddCheck = "" + extraAddCheckBox.isChecked();
        switch (extraAddCheck) {
            case "true":
                mExtraAdd = 1.07;
                break;
            case "false":
                mExtraAdd = 1;
                break;
            default:
                break;
        }

        return mExtraAdd;
    }

    //从RadioGroup中获得战斗评价系数evaluateExp
    public double getEvaluateExp() {
        double mEvaluateExp = 0;
        RadioGroup evaluateGroup = (RadioGroup) findViewById(R.id.evaluate_group);
        RadioButton evaluateButton = (RadioButton) findViewById(evaluateGroup.getCheckedRadioButtonId());
        String evaluate = evaluateButton.getText().toString();
        if (evaluate != null && evaluate.length() != 0) {
            switch (evaluate) {
                case "SS":
                    mEvaluateExp = 1;
                    break;
                case "S":
                    mEvaluateExp = 1;
                    break;
                case "A":
                    mEvaluateExp = 10.0 / 12.0;
                    break;
                case "B":
                    mEvaluateExp = 8.0 / 12;
                    break;
                case "C":
                    mEvaluateExp = 7.0 / 12;
                    break;
                case "D":
                    mEvaluateExp = 5.0 / 12;
                    break;
                default:
                    break;
            }
        }
        return mEvaluateExp;

    }

    //从RadioGroup中获取关卡经验值
    public int getStageExp() {
        int mStageExp = 0;
        MyRadioGroup stageRadioGroup = (MyRadioGroup) findViewById(R.id.stage_radio_group);
        RadioButton stageRadioButton = (RadioButton) findViewById(stageRadioGroup.getCheckedRadioButtonId());
        String stage = stageRadioButton.getText().toString();
        if (stage != null && stage.length() != 0) {
            switch (stage) {
                case "1-1":
                    mStageExp = 36;
                    break;
                case "1-2":
                    mStageExp = 60;
                    break;
                case "1-3":
                    mStageExp = 96;
                    break;
                case "1-4":
                    mStageExp = 144;
                    break;
                case "2-1":
                    mStageExp = 120;
                    break;
                case "2-2":
                    mStageExp = 144;
                    break;
                case "2-3":
                    mStageExp = 180;
                    break;
                case "2-4":
                    mStageExp = 240;
                    break;
                case "2-5":
                    mStageExp = 480;
                    break;
                case "2-6":
                    mStageExp = 1;
                    break;
                case "3-1":
                    mStageExp = 180;
                    break;
                case "3-2":
                    mStageExp = 216;
                    break;
                case "3-3":
                    mStageExp = 264;
                    break;
                case "3-4":
                    mStageExp = 360;
                    break;
                case "4-1":
                    mStageExp = 300;
                    break;
                case "4-2":
                    mStageExp = 360;
                    break;
                case "4-3":
                    mStageExp = 396;
                    break;
                case "4-4":
                    mStageExp = 432;
                    break;
                case "5-1":
                    mStageExp = 420;
                    break;
                case "5-2":
                    mStageExp = 444;
                    break;
                case "5-3":
                    mStageExp = 480;
                    break;
                case "5-4":
                    mStageExp = 504;
                    break;
                case "5-5":
                    mStageExp = 540;
                    break;
                case "6-1":
                    mStageExp = 480;
                    break;
                case "6-2":
                    mStageExp = 492;
                    break;
                case "6-3":
                    mStageExp = 504;
                    break;
                case "6-4":
                    mStageExp = 600;
                    break;
                case "7-1":
                    mStageExp = 1;
                    break;
                case "7-2":
                    mStageExp = 1;
                    break;
                default:
                    break;
            }
        }
        return mStageExp;
    }

    //从RadioGroup中获取关卡标签
    public String getStage() {
        String mStage = "";
        MyRadioGroup stageRadioGroup = (MyRadioGroup) findViewById(R.id.stage_radio_group);
        RadioButton stageRadioButton = (RadioButton) findViewById(stageRadioGroup.getCheckedRadioButtonId());
        mStage = stageRadioButton.getText().toString();
        if (mStage != null && mStage.length() != 0) {
        } else {
            mStage = "数据出错！";
        }
        return mStage;
    }

    //从RadioGroup中获取战斗评价
    public String getEvaluate() {
        String mEvaluate = "";
        RadioGroup evaluateRadioGroup = (RadioGroup) findViewById(R.id.evaluate_group);
        RadioButton evaluateRadioButton = (RadioButton) findViewById(evaluateRadioGroup.getCheckedRadioButtonId());
        mEvaluate = evaluateRadioButton.getText().toString();
        if (mEvaluate != null && mEvaluate.length() != 0) {
        } else {
            mEvaluate = "数据出错！";
        }
        return mEvaluate;
    }
}
