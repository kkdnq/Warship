package com.example.android.warship;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by hasee on 2017/9/4.
 */

public class EquipmentListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment_list);

        //绑定Adapter
        ((ListView) findViewById(R.id.eq_list)).setAdapter(new EquipmentAdapter(this, getList()));

    }

    @Override
    protected void onResume() {
        //绑定Adapter
        ((ListView) findViewById(R.id.eq_list)).setAdapter(new EquipmentAdapter(this, getList()));
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_eq_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //右上角菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //切换到shipList
            case R.id.list_eq_swap:
                Intent intent1 = new Intent(EquipmentListActivity.this,ShipListActivity.class);
                startActivity(intent1);
                finish();
                break;
            //添加一条装备数据
            case R.id.list_eq_add:
                int resultCode = 201;
                Intent intent = new Intent(EquipmentListActivity.this, EquipmentInsertActivity.class);
                startActivityForResult(intent, resultCode);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<ShipEquip> getList(){
        int columnIndex = 0;
        String otherData[] = getResources().getStringArray(R.array.eqShowInList);
        ArrayList<ShipEquip> arrayList = new ArrayList<ShipEquip>();
        //从数据库读取cursor
        String columns = "BIANHAO,ZHUANGBEIMING,ZHUANGBEIZHONGLEI,XINGJI,ICON";
        String tables = "equipments";
        String orders = "BIANHAO";
        DbOperation operation = new DbOperation();
        Cursor cursor = operation.read(columns,tables,null,orders);
        //将数据从cursor中读取出来并存入arrayList
        if (!cursor.moveToFirst()){
        }else {
            do {
                //获取头像
                columnIndex = cursor.getColumnIndex("ICON");
                byte[] icon = cursor.getBlob(columnIndex);
                Bitmap bmp = BitmapFactory.decodeByteArray(icon,0,icon.length);
                //获取其他数据
                String listData[] = new String[4];
                for (int i = 0;i < otherData.length;i ++){
                    columnIndex = cursor.getColumnIndex(otherData[i]);
                     listData[i] = cursor.getString(columnIndex).toString();
                }
                //将数据存入arrayList
                arrayList.add(new ShipEquip(bmp,listData));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }
}