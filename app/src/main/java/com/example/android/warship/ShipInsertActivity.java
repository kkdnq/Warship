package com.example.android.warship;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by K.K on 2017/8/7.
 */

public class ShipInsertActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship_insert);

        //绑定OnClickListener来插入图片
        ((ImageView) findViewById(R.id.pic_0)).setOnClickListener(picListener);
        ((ImageView) findViewById(R.id.pic_1)).setOnClickListener(picListener);
        //填充Spinner
        setSpinner();
    }

    //填充Spinner
    private void setSpinner() {
        //填充shecheng spinner
        String shecheng[] = getResources().getStringArray(R.array.shecheng);
        ArrayList shecheng_list = new ArrayList<String>();
        for (int i = 0; i < shecheng.length; i++) {
            shecheng_list.add(shecheng[i]);
        }
        ((Spinner) findViewById(R.id.insert_shecheng)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shecheng_list));

        //填充jianzhong spinner
        String jianzhong[] = getResources().getStringArray(R.array.jianzhong);
        ArrayList jianzhong_list = new ArrayList<String>();
        for (int i = 0; i < jianzhong.length; i++) {
            jianzhong_list.add(jianzhong[i]);
        }
        ((Spinner) findViewById(R.id.insert_jianzhong)).setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jianzhong_list));

        //填充zhuangbei Spinner
        ArrayList<String> zhuangbei_list = getList();
        ((Spinner) findViewById(R.id.insert_zhuangbei1)).setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,zhuangbei_list));
        ((Spinner) findViewById(R.id.insert_zhuangbei2)).setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,zhuangbei_list));
        ((Spinner) findViewById(R.id.insert_zhuangbei3)).setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,zhuangbei_list));
        ((Spinner) findViewById(R.id.insert_zhuangbei4)).setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,zhuangbei_list));
    }

    //插入图片
    private View.OnClickListener picListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String bianhao = ((EditText) findViewById(R.id.insert_bianhao)).getText().toString();
            GetPic getPic = new GetPic();
            if (bianhao == null || bianhao.length() == 0) {
                Toast.makeText(getApplicationContext(), "请正确输入编号", Toast.LENGTH_SHORT).show();
            } else {
                switch (v.getId()) {
                    case R.id.pic_0:
                        Bitmap bmp0 = getPic.getPic(R.id.pic_0, bianhao);
                        if (bmp0 == null) {
                            Toast.makeText(getApplicationContext(), "图片不存在！", Toast.LENGTH_SHORT).show();
                        } else {
                            ((ImageView) findViewById(R.id.pic_0)).setImageBitmap(bmp0);
                        }
                        break;
                    case R.id.pic_1:
                        Bitmap bmp1 = getPic.getPic(R.id.pic_1, bianhao);
                        if (bmp1 == null) {
                            Toast.makeText(getApplicationContext(), "图片不存在！", Toast.LENGTH_SHORT).show();
                        } else {
                            ((ImageView) findViewById(R.id.pic_1)).setImageBitmap(bmp1);
                        }
                        break;
                }
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_insert, menu);
        return true;
    }

    //右上角菜单
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //确认 创建一条数据 按钮
            case R.id.insert_done:
                Boolean bo = insert();
                if (bo) {
                    int resultCode0 = 2;
                    Intent intent0 = new Intent();
                    intent0.putExtra("result", "insert");
                    setResult(resultCode0, intent0);
                    finish();
                    break;
                } else {
                    Toast.makeText(this, "请检查数据", Toast.LENGTH_SHORT);
                }
                //取消创建数据并返回ListActivity
            case R.id.insert_cancel:
                int resultCode1 = 3;
                Intent intent1 = new Intent();
                intent1.putExtra("result", "cancel");
                setResult(resultCode1, intent1);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    //创建一条数据
    private boolean insert() {
        int id = 0;
        boolean result = true;

        //获取数据并打包
        ContentValues values = new ContentValues();
        String editList[] = getResources().getStringArray(R.array.editList);
        String spinnerList[] = getResources().getStringArray(R.array.spinnerList);

        //获取edit数据
        for (int i = 0; i < editList.length; i++) {
            id = getResources().getIdentifier("insert_" + editList[i], "id", getBaseContext().getPackageName());
            String value = ((EditText) findViewById(id)).getText().toString();
            if (value != null & value.length() != 0) {
                values.put(editList[i].toUpperCase(), value);
                result = true;
            } else {
                result = false;
            }
        }
        //获取spinner数据
        for (int i = 0; i < spinnerList.length; i++) {
            id = getResources().getIdentifier("insert_" + spinnerList[i], "id", getBaseContext().getPackageName());
            String value = ((Spinner) findViewById(id)).getSelectedItem().toString();
            if (value != null & value.length() != 0) {
                values.put(spinnerList[i].toUpperCase(), value);
                result = true;
            } else {
                result = false;
            }
        }
        //获取图片数据
        ByteArrayOutputStream baos0 = new ByteArrayOutputStream();
        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        Bitmap bitmap0 = ((BitmapDrawable) ((ImageView) findViewById(R.id.pic_0)).getDrawable()).getBitmap();
        Bitmap bitmap1 = ((BitmapDrawable) ((ImageView) findViewById(R.id.pic_1)).getDrawable()).getBitmap();
        bitmap0.compress(Bitmap.CompressFormat.PNG, 100, baos0);
        bitmap1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
        values.put("PICTURE", baos0.toByteArray());
        values.put("ICON", baos1.toByteArray());

        //检查并插入数据
        if (result) {
            DbOperation operation = new DbOperation();
            operation.insert("ships", values);
        }
        return result;
    }

    //获取数组
    private ArrayList<String> getList(){
        ArrayList<String> arrayList = new ArrayList<String>();
        int columnIndex = 0;
        String columns = "ZHUANGBEIMING";
        String tables = "equipments";
        DbOperation operation = new DbOperation();
        Cursor cursor = operation.read(columns,tables,null,null);
        if (!cursor.moveToFirst()){
        }else {
            do {
                columnIndex = cursor.getColumnIndex("ZHUANGBEIMING");
                arrayList.add(cursor.getString(columnIndex).toString());
            } while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }
}
