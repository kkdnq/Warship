package com.example.android.warship;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by hasee on 2017/9/4.
 */

public class DbOperation {

    public boolean insert(String tableName, ContentValues values){
        ShipDbHelper shelter = new ShipDbHelper(MyApplication.getGlobalContext());
        SQLiteDatabase db = shelter.getWritableDatabase();
        db.insert(tableName,null,values);
        return true;
    }

    public Cursor read(String columns,String tables,String ranges,String orders){
        String sql_ranges = "";
        //检查拼接ranges部分
        if (ranges != null && ranges.length() != 0){
            sql_ranges = " WHERE BIANHAO = " + ranges;
        }
        //检查拼接orders部分
        String sql_order = " ORDER BY ";
        if (orders != null && orders.length() != 0){
            sql_order += orders;
        }else {
            sql_order += "BIANHAO";
        }
        //最终拼接出SQL语句
        String SQL_READ = "SELECT " + columns + " FROM " + tables + sql_ranges + sql_order + " ;";
//        Log.v("sqllllllllllllllll",""+SQL_READ);

        //从数据库读取
        ShipDbHelper shelter = new ShipDbHelper(MyApplication.getGlobalContext());
        SQLiteDatabase db = shelter.getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_READ,null);
        return cursor;
    }

    public boolean delete(String tables,String bianhao){
        String SQL_DELETE = "DELETE FROM " + tables + " WHERE BIANHAO = " + bianhao + ";";
        ShipDbHelper shelter = new ShipDbHelper(MyApplication.getGlobalContext());
        SQLiteDatabase db = shelter.getWritableDatabase();
        db.execSQL(SQL_DELETE);
        return true;
    }

    public boolean update(){
        return true;
    }
}
