package com.example.android.warship;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hasee on 2017/9/4.
 */

public class GetPic {

    public Bitmap getPic(int id_Pic, String bianhao) {
        Bitmap bmp = null;
            if (isSdCardExist()) {
                switch (id_Pic) {
                    case R.id.pic_0:
                        String picName = "s_normal_" + bianhao + ".png";
                        bmp = BitmapFactory.decodeFile(getImagePath(picName).toString());
                        break;
                    case R.id.pic_1:
                        String iconName = "ic_normal_" + bianhao + ".png";
                        bmp = BitmapFactory.decodeFile(getImagePath(iconName).toString());
                        break;
                    case R.id.pic_eq:
                        String iconEqName = "equip_large_" + bianhao + ".png";
                        bmp = BitmapFactory.decodeFile(getImagePath(iconEqName).toString());
                }
            }
        return bmp;
    }

    //判断是否为String数字
    private boolean isNumber(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches() || str.length() == 0 || str == null) {
            return false;
        }
        return true;
    }

    //判断SD卡是否存在
    private static boolean isSdCardExist() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    //获得Image的路径
    private String getImagePath(String picName) {
        String filepath = "";
        File file = new File(Environment.getExternalStorageDirectory().toString() + "/Download", picName);
        if (file.exists()) {
            filepath = file.getAbsolutePath();
        } else {
            Toast.makeText(MyApplication.getGlobalContext(), "图片不存在！", Toast.LENGTH_SHORT).show();
        }
        return filepath;
    }
}
